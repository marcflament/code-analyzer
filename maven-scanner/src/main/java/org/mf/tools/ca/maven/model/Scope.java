package org.mf.tools.ca.maven.model;

public enum Scope {
	COMPILE,
	RUNTIME,
	TEST,
	SYSTEM,
	PROVIDED,
	IMPORT;

	public static Scope resolve(String s) {
		if (s == null)
			return COMPILE;
		return valueOf(s.toUpperCase());
	}

	@Override
	public String toString() {
		return name().toLowerCase();
	}

}