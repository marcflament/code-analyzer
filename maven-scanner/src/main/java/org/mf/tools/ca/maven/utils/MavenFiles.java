package org.mf.tools.ca.maven.utils;

import java.io.File;

import org.apache.maven.settings.Settings;
import org.apache.maven.settings.building.DefaultSettingsBuilder;
import org.apache.maven.settings.building.DefaultSettingsBuilderFactory;
import org.apache.maven.settings.building.DefaultSettingsBuildingRequest;
import org.apache.maven.settings.building.SettingsBuilder;
import org.apache.maven.settings.building.SettingsBuildingException;
import org.apache.maven.settings.building.SettingsBuildingRequest;
import org.apache.maven.settings.building.SettingsBuildingResult;

/**
 * @author Marc Flament
 * @created 2018-10-27
 */
public class MavenFiles {

	private static volatile MavenFiles instance;

	private static MavenFiles getInstance() {
		if (instance == null) {
			synchronized (MavenFiles.class) {
				if (instance == null)
					instance = new MavenFiles();
			}
		}
		return instance;
	}

	public static File mavenHome() {
		return getInstance().m2Home;
	}

	public static File globalSettings() {
		return getInstance().globalSettings;
	}

	public static File userSettings() {
		return getInstance().userSettings;
	}

	public static File localRepository() {
		return getInstance().getLocalRepository();
	}

	public static Settings mavenSettings() {
		return getInstance().getSettings();
	}

	private final File m2Home;
	private final File globalSettings;
	private final File userSettings;

	private volatile Settings mavenSettings;

	private MavenFiles() {
		super();
		m2Home = getMavenHome();
		globalSettings = getGlobalSettings(m2Home);
		userSettings = getUserSettings();
	}

	private static File getMavenHome() {
		String env = System.getenv("M2_HOME");
		if (env == null)
			env = System.getenv("MAVEN_HOME");
		if (env == null)
			throw new IllegalStateException(
					"Maven home was not found, create either M2_HOME or MAVEN_HOME encironement variable pointing to your maven installation");
		File res = new File(env);
		if (!res.exists())
			throw new IllegalStateException("Maven home " + res + " does no exists");
		if (!res.isDirectory())
			throw new IllegalStateException("Maven home " + res + " is not a directory");
		return res;
	}

	private static File getGlobalSettings(File m2Home) {
		File res = new File(m2Home, "conf/settings.xml");
		if (!res.exists())
			throw new IllegalStateException("Maven global settings " + res + " does no exists");
		if (!res.isFile())
			throw new IllegalStateException("Maven global settings " + res + " is not a file");
		return res;
	}

	private static File getUserSettings() {
		return new File(System.getProperty("user.home"), ".m2/settings.xml");
	}

	private Settings getSettings() {
		if (mavenSettings == null) {
			synchronized (this) {
				if (mavenSettings == null) {
					mavenSettings = loadSettings();
				}
			}
		}
		return mavenSettings;
	}

	private Settings loadSettings() {
		SettingsBuilder settingsBuilder = createSettingsBuilder();
		SettingsBuildingRequest request = createSettingsBuildingRequest();
		try {
			SettingsBuildingResult result = settingsBuilder.build(request);
			if (!result.getProblems().isEmpty())
				throw new IllegalStateException("Problems in maven settings " + result.getProblems());
			return result.getEffectiveSettings();
		} catch (SettingsBuildingException e) {
			throw new IllegalStateException("Error building maven settings", e);
		}
	}

	private File getLocalRepository() {
		String localRepository = getSettings().getLocalRepository();
		if (localRepository == null)
			localRepository = defaultLocalRepository();
		File res = new File(localRepository);
		if (!res.exists())
			throw new IllegalStateException("Local repository " + res + " does not exists");
		if (!res.isDirectory())
			throw new IllegalStateException("Local repository " + res + " is not a directory");
		return res;
	}

	private static String defaultLocalRepository() {
		return System.getProperty("user.home") + "/.m2/repository";
	}

	private SettingsBuildingRequest createSettingsBuildingRequest() {
		DefaultSettingsBuildingRequest request = new DefaultSettingsBuildingRequest();
		request.setGlobalSettingsFile(globalSettings);
		request.setUserSettingsFile(userSettings);
		request.setSystemProperties(System.getProperties());
		return request;
	}

	private static DefaultSettingsBuilder createSettingsBuilder() {
		return new DefaultSettingsBuilderFactory().newInstance();
	}

}
