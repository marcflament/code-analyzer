package org.mf.tools.ca.maven.model;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Marc Flament
 * @created 2018-10-27
 */
public class MavenDependency implements DependenciesContainer {

	private final MavenArtifact artifact;

	private final Scope scope;

	private final Set<MavenDependency> dependencies = new LinkedHashSet<>();

	private final boolean optional;

	public MavenDependency(MavenArtifact artifact, Scope scope, boolean optional) {
		this.artifact = Objects.requireNonNull(artifact, "artifact is null");
		this.scope = scope == null ? Scope.COMPILE : scope;
		this.optional = optional;
	}

	public Scope getScope() {
		return scope;
	}

	public boolean isOptional() {
		return optional;
	}

	public MavenArtifact getArtifact() {
		return artifact;
	}

	@Override
	public Iterator<MavenDependency> iterator() {
		return dependencies.iterator();
	}

	@Override
	public Set<MavenDependency> getDependencies() {
		return dependencies;
	}

	@Override
	public void addDependency(MavenDependency dependency) {
		this.dependencies.add(dependency);
	}

	/**
	 * parse artifact id from
	 * groupId:artifactId:packaging[:classifier]:version:scope
	 */
	public static MavenArtifactId parseArtifactId(String input) {
		int index = input.lastIndexOf(':');
		if (index < 0)
			throw new IllegalArgumentException("Invalid dependency " + input + ", no ':' separator");
		String artifactInput = input.substring(0, index);
		return MavenArtifactId.parse(artifactInput);
	}

	/**
	 * parse scope from groupId:artifactId:packaging[:classifier]:version:scope
	 */
	public static Scope parseScope(String input) {
		int index = input.lastIndexOf(':');
		if (index < 0)
			throw new IllegalArgumentException("Invalid dependency " + input + ", no ':' separator");
		String scopeName = input.substring(index + 1);
		return Scope.resolve(scopeName);
	}

	@Override
	public String toString() {
		String res = String.format("%s:%s", artifact.getId(), scope);
		if (optional)
			res += " (optional)";
		if (artifact instanceof MissingArtifact)
			res += " (missing)";
		return res;
	}
}
