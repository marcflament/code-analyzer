package org.mf.tools.ca.maven.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Consumer;

import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.mf.tools.ca.utils.io.SafeIOUtils;
import org.mf.tools.ca.utils.io.TempFileProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper for maven invoker api.<br/>
 * Used to invoke maven goals on a maven project.
 * 
 * @author Marc Flament
 * @created 2018-10-27
 */
public class MavenInvoker {

	public static final class StringHolder implements Consumer<String> {
		private String string;

		@Override
		public void accept(String t) {
			this.string = t;
		};

		public String get() {
			return string;
		}
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(MavenInvoker.class);

	private final File projectFile;

	private final Invoker invoker;

	private final List<String> goals = new ArrayList<>();

	private final Properties properties = new Properties();

	private final Map<String, Consumer<String>> textOutputHandlers = new HashMap<>();


	public static MavenInvoker forProject(MavenProject project) {
		return forProject(project.getBasedir());
	}

	public static MavenInvoker forProject(File projectDirectory) {
		return new MavenInvoker(new File(projectDirectory, "pom.xml"), MavenFiles.mavenHome().getAbsolutePath());
	}

	public static MavenInvoker forPom(File pomFile) {
		return new MavenInvoker(pomFile, MavenFiles.mavenHome().getAbsolutePath());
	}

	public static MavenInvoker forPom(File pomFile, String mavenHome) {
		return new MavenInvoker(pomFile, mavenHome);
	}

	private MavenInvoker(File projectFile, String mavenHome) {
		this.projectFile = projectFile;
		invoker = new DefaultInvoker();
		invoker.setMavenHome(new File(Objects.requireNonNull(mavenHome,
				"maven home is null, check the MAVEN_HOME environement variable, or provide one.")));
		invoker.setOutputHandler(LOGGER::debug);
	}

	public MavenInvoker goals(String... goals) {
		this.goals.addAll(Arrays.asList(goals));
		return this;
	}

	public MavenInvoker property(String key, String value) {
		this.properties.put(key, value);
		return this;
	}

	/**
	 * Register a property that is used by any goals dumping text to an output file.
	 * The file will be created as a temp file deleted on exit.
	 */
	public MavenInvoker textOutput(String key, Consumer<String> handler) {
		textOutputHandlers.put(key, handler);
		return this;
	}

	public int invoke() {
		InvocationRequest request = new DefaultInvocationRequest();
		request.setPomFile(projectFile);
		request.setGoals(goals);
		request.setBatchMode(true); // interactive mode off

		Map<String, File> outputFiles;
		if (!textOutputHandlers.isEmpty()) {
			outputFiles = new HashMap<>(textOutputHandlers.size());
			textOutputHandlers.keySet().stream().forEach(k -> {
				File tempFile = TempFileProvider.createTempFile(false);
				outputFiles.put(k, tempFile);
				properties.put(k, tempFile.getAbsolutePath());
			});
		} else {
			outputFiles = Collections.emptyMap();
		}

		request.setProperties(properties);
		InvocationResult result;
		try {
			result = invoker.execute(request);
		} catch (Exception e) {
			throw new IllegalStateException("Error executing maven goals " + goals + " for project " + projectFile, e);
		}

		if (result.getExecutionException() != null) {
			throw new IllegalStateException("Error executing maven goals " + goals + " for project " + projectFile,
					result.getExecutionException());
		}

		outputFiles.forEach((k, f) -> {
			Consumer<String> handler = textOutputHandlers.get(k);
			handler.accept(SafeIOUtils.toString(f));
			f.delete();
		});

		return result.getExitCode();
	}

}
