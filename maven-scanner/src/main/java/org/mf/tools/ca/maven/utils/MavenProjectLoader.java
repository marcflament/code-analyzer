package org.mf.tools.ca.maven.utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

/**
 * Load maven project model from a pom.xml.
 * 
 * @author Marc Flament
 * @created 2018-10-27
 */
public class MavenProjectLoader {

	public static final String POM_XML = "pom.xml";

	private final MavenXpp3Reader mavenreader = new MavenXpp3Reader();

	public MavenProject loadMavenProject(File file) {
		File pomFile = file.isDirectory() ? new File(file, POM_XML) : file;
		if (!pomFile.exists())
			throw new IllegalArgumentException("file " + pomFile + " not found");

		try (FileReader reader = new FileReader(pomFile)) {
			Model model = mavenreader.read(reader);
			MavenProject res = new MavenProject(model);
			res.setFile(pomFile);
			return res;
		} catch (IOException e) {
			throw new IllegalStateException("Error loading maven project " + pomFile, e);
		} catch (XmlPullParserException e) {
			throw new IllegalStateException("Error reading maven project " + pomFile, e);
		}
	}
}
