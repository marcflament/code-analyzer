package org.mf.tools.ca.maven.scanner.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.DependencyManagement;
import org.apache.maven.model.Exclusion;
import org.apache.maven.project.MavenProject;
import org.mf.tools.ca.maven.model.MavenVersionedProjectId;
import org.mf.tools.ca.maven.model.Scope;

/**
 * @author Marc Flament
 * @created 2018-11-04
 */
public class EffectiveMavenProjectFactory {

	private static final Pattern PROPERTY_PATTERN = Pattern.compile("\\$\\{([^\\}]+)\\}");

	private final MavenProjectRepository projectRepository;

	public EffectiveMavenProjectFactory(MavenProjectRepository projectRepository) {
		super();
		this.projectRepository = projectRepository;
	}

	public EffectiveMavenProject create(MavenProject project) {
		Map<String, Dependency> managedDependencies = createManagedDependencies(project);
		List<Dependency> dependencies = createDependencies(project, managedDependencies);
		return new EffectiveMavenProject(project, managedDependencies, dependencies);
	}

	private List<Dependency> createDependencies(MavenProject project, Map<String, Dependency> managedDependencies) {
		List<Dependency> res = new LinkedList<>();
		Set<String> keys = new LinkedHashSet<>();
		visitAncestors(project, p -> {
			p.getDependencies()
				.stream()
				.map(d -> resolveProperties(p, d))
				.filter(d -> keys.add(d.getManagementKey()))
				.forEach(res::add);
		});
		return res;
	}

	private Map<String, Dependency> createManagedDependencies(MavenProject project) {
		Map<String, Dependency> res = new HashMap<>();
		visitAncestors(project, p -> {
			DependencyManagement dependencyManagement = p.getDependencyManagement();
			if (dependencyManagement != null) {
				dependencyManagement.getDependencies()
					.stream()
					.map(d -> resolveProperties(project, d))
					.flatMap(this::expandBOM)
					.forEach(d -> res.put(d.getManagementKey(), d));
			}
		});
		return res;
	}

	private Stream<Dependency> expandBOM(Dependency dependency) {
		Collection<Dependency> dependencies;
		if (Scope.resolve(dependency.getScope()) == Scope.IMPORT) {
			dependencies = loadBOM(dependency);
		} else {
			dependencies = Collections.singleton(dependency);
		}
		return dependencies.stream();
	}

	private Collection<Dependency> loadBOM(Dependency dependency) {
		EffectiveMavenProject bomProject = projectRepository.getProject(MavenVersionedProjectId.from(dependency))
			.map(this::create)
			.orElse(null);

		if (bomProject == null)
			return Collections.emptyList();

		return bomProject.getManagedDependencies().values();
	}

	private Dependency resolveProperties(MavenProject project, Dependency dependency) {
		Dependency res = dependency.clone();
		res.setGroupId(resolveProperty(project, dependency.getGroupId()));
		res.setArtifactId(resolveProperty(project, dependency.getArtifactId()));
		res.setVersion(resolveProperty(project, dependency.getVersion()));
		res.setType(resolveProperty(project, dependency.getType()));
		res.setClassifier(resolveProperty(project, dependency.getClassifier()));
		res.setScope(resolveProperty(project, dependency.getScope()));
		res.setOptional(resolveProperty(project, dependency.getOptional()));
		if (!dependency.getExclusions().isEmpty()) {
			List<Exclusion> exclusions = new ArrayList<>(dependency.getExclusions().size());
			dependency.getExclusions().stream().map(e -> resolveProperties(project, e)).forEach(exclusions::add);
			res.setExclusions(exclusions);
		}
		return res;
	}

	private Exclusion resolveProperties(MavenProject project, Exclusion exclusion) {
		Exclusion res = new Exclusion();
		res.setGroupId(resolveProperty(project, exclusion.getGroupId()));
		res.setArtifactId(resolveProperty(project, exclusion.getArtifactId()));
		return res;
	}

	private String resolveProperty(MavenProject project, String input) {
		if (input == null)
			return null;

		String key = getPropertyKey(input);
		if (key != null) {
			String property = getProperty(project, key);
			// resolved property can be another property
			return resolveProperty(project, property);
		}

		return input;
	}

	private static String getProperty(MavenProject project, String key) {
		// System properties come first
		String property = System.getProperty(key);
		if (property != null)
			return property;

		// project fields (ie: project.artifactId) can not be overrode by
		// project properties (except if this field does not exists, ie: project.foo)
		String propertyName = stripProperty(key, "project.");
		if (propertyName != null) {
			property = beanProperty(project, propertyName);
			if (property != null)
				return property;
		}

		// project properties can override environment variables
		property = getProjectProperty(project, key);
		if (property != null)
			return property;

		// project properties can override environment variables
		propertyName = stripProperty(key, "env.");
		if (propertyName != null) {
			property = System.getenv(propertyName);
			if (property != null)
				return property;
		}

		// should also handle ${settings.xxx} but I was unable to get it working in
		// groupId, artifactId or scope
		return null;
	}

	private static String getProjectProperty(MavenProject project, String key) {
		MavenProject current = project;
		while (current != null) {
			String property = current.getProperties().getProperty(key);
			if (property != null)
				return property;
			current = current.getParent();
		}
		return null;
	}

	private static void visitAncestors(MavenProject project, Consumer<MavenProject> visitor) {
		MavenProject current = project;
		while (current != null) {
			visitor.accept(current);
			current = current.getParent();
		}
	}

	private static String getPropertyKey(String input) {
		Matcher matcher = PROPERTY_PATTERN.matcher(input);
		if (matcher.matches())
			return matcher.group(1);
		return null;
	}

	private static String beanProperty(Object bean, String property) {
		Object res;
		try {
			res = BeanUtils.getProperty(bean, property);
		} catch (Exception e) {
			res = null;
		}
		return res == null ? null : res.toString();
	}

	private static String stripProperty(String input, String prefix) {
		if (input.startsWith(prefix))
			return input.substring(prefix.length());
		return null;
	}
}
