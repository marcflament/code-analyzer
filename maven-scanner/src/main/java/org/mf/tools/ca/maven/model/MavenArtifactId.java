package org.mf.tools.ca.maven.model;

import java.util.Arrays;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.maven.model.Dependency;
import org.apache.maven.project.MavenProject;

/**
 * @author Marc Flament
 * @created 2018-10-28
 */
public class MavenArtifactId {

	private final MavenVersionedProjectId versionedProjectId;

	private final String classifier;

	private final int hashCode;

	public MavenArtifactId(MavenVersionedProjectId projectId) {
		this(projectId, null);
	}

	public MavenArtifactId(MavenVersionedProjectId projectId, String classifier) {
		super();
		this.versionedProjectId = projectId;
		this.classifier = classifier;
		this.hashCode = new HashCodeBuilder().append(projectId).append(classifier).toHashCode();
	}

	public MavenProjectId getProjectId() {
		return versionedProjectId.getProjectId();
	}

	public MavenVersionedProjectId getVersionedProjectId() {
		return versionedProjectId;
	}

	public String getPackaging() {
		return versionedProjectId.getPackaging();
	}

	public String getGroupId() {
		return versionedProjectId.getGroupId();
	}

	public String getArtifactId() {
		return versionedProjectId.getArtifactId();
	}

	public String getVersion() {
		return versionedProjectId.getVersion();
	}

	public String getClassifier() {
		return classifier;
	}

	public boolean hasClassifier() {
		return classifier != null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(versionedProjectId.getGroupId()).append(":").append(versionedProjectId.getArtifactId());
		sb.append(":").append(versionedProjectId.getPackaging());
		if (hasClassifier())
			sb.append(":").append(classifier);
		sb.append(":").append(versionedProjectId.getVersion());
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MavenArtifactId other = (MavenArtifactId) obj;
		return new EqualsBuilder().append(versionedProjectId, other.versionedProjectId)
			.append(classifier, other.classifier)
			.isEquals();
	}

	/**
	 * The relative path if this artifact inside a repository
	 */
	public String repositoryFile() {
		String fileName = versionedProjectId.getArtifactId() + "-" + versionedProjectId.getVersion();
		if (hasClassifier())
			fileName += "-" + classifier;
		fileName += "." + versionedProjectId.getPackaging();
		return versionedProjectId.repositoryPath() + "/" + fileName;
	}

	/**
	 * groupId:artifactId:packaging[:classifier]:version
	 */
	public static MavenArtifactId parse(String input) {
		return parse(input.split(":"));
	}

	/**
	 * groupId,artifactId,packaging,[classifier],version
	 */
	public static MavenArtifactId parse(String[] parts) {
		if (parts.length < 4)
			throw new IllegalArgumentException("Missing parts in " + Arrays.toString(parts)
					+ ",expected at least 4 parts");
		if (parts.length > 5)
			throw new IllegalArgumentException("Too many parts in " + Arrays.toString(parts)
					+ ",expected at most 5 parts");

		int index = 0;
		String groupId = parts[index++];
		String artifactId = parts[index++];
		String packaging = parts[index++];
		String classifier = parts.length == 4 ? null : parts[index++];
		String version = parts[index++];
		MavenProjectId projectId = new MavenProjectId(groupId, artifactId, packaging);
		MavenVersionedProjectId versionedProjectId = new MavenVersionedProjectId(projectId, version);
		return new MavenArtifactId(versionedProjectId, classifier);
	}

	public static MavenArtifactId from(MavenProject mavenProject) {
		return new MavenArtifactId(MavenVersionedProjectId.from(mavenProject), null);
	}

	public static MavenArtifactId from(Dependency mavenDependency) {
		return new MavenArtifactId(MavenVersionedProjectId.from(mavenDependency), mavenDependency.getClassifier());
	}

}
