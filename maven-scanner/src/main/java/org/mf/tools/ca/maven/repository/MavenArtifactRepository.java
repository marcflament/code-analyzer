package org.mf.tools.ca.maven.repository;

import java.util.Optional;

import org.mf.tools.ca.maven.model.MavenArtifact;
import org.mf.tools.ca.maven.model.MavenArtifactId;

public interface MavenArtifactRepository extends Iterable<MavenArtifact> {

	boolean contains(MavenArtifactId id);

	Optional<MavenArtifact> get(MavenArtifactId id);

}
