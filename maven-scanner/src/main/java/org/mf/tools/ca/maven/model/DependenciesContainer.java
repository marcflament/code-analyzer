package org.mf.tools.ca.maven.model;

import java.util.Iterator;
import java.util.Set;

public interface DependenciesContainer extends Iterable<MavenDependency> {

	Set<MavenDependency> getDependencies();

	void addDependency(MavenDependency dependency);
	
	@Override
	default Iterator<MavenDependency> iterator() {
		return getDependencies().iterator();
	}
}
