package org.mf.tools.ca.maven.scanner.repository;

import java.util.List;
import java.util.Map;

import org.apache.maven.model.Dependency;
import org.apache.maven.project.MavenProject;

public class EffectiveMavenProject {

	private final MavenProject project;

	private final Map<String, Dependency> managedDependencies;

	private final List<Dependency> dependencies;

	public EffectiveMavenProject(MavenProject project, Map<String, Dependency> managedDependencies,
			List<Dependency> dependencies) {
		super();
		this.project = project;
		this.managedDependencies = managedDependencies;
		this.dependencies = dependencies;
	}

	public MavenProject getProject() {
		return project;
	}

	public List<Dependency> getDependencies() {
		return dependencies;
	}

	public Map<String, Dependency> getManagedDependencies() {
		return managedDependencies;
	}

	public Dependency getManagedDependency(String managementKey) {
		return managedDependencies.get(managementKey);
	}
	
	public List<Dependency> getMergedDependencies() {
		return dependencies;
	}
	
	public Dependency mergeDependency(Dependency dependency) {
		Dependency managedDependency = managedDependencies.get(dependency.getManagementKey());
		if (managedDependency == null)
			return dependency;
		return mergeDependency(managedDependency, dependency);
	}

	private static Dependency mergeDependency(Dependency source, Dependency target) {
		if (target.getVersion() == null)
			target.setVersion(source.getVersion());
		if (target.getScope() == null)
			target.setScope(source.getScope());
		if (target.getOptional() == null)
			target.setOptional(source.getOptional());
		if (target.getSystemPath() == null)
			target.setSystemPath(source.getSystemPath());
		if (target.getExclusions().isEmpty())
			target.setExclusions(source.getExclusions());
		return target;
	}
}
