package org.mf.tools.ca.maven.model;

import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.model.Dependency;
import org.apache.maven.project.MavenProject;

/**
 * @author Marc Flament
 * @created 2018-10-28
 */
public class MavenVersionedProjectId {

	private final MavenProjectId projectId;

	private final String version;

	private final ArtifactVersion parsedVersion;

	private final int hashCode;

	public MavenVersionedProjectId(MavenProjectId projectId, String version) {
		super();
		this.projectId = Objects.requireNonNull(projectId, "projectId is null");
		this.version = version;
		this.parsedVersion = version == null ? null : new DefaultArtifactVersion(version);
		this.hashCode = new HashCodeBuilder().append(projectId).append(version).toHashCode();
	}

	public MavenProjectId getProjectId() {
		return projectId;
	}

	public String getVersion() {
		return version;
	}

	public ArtifactVersion getParsedVersion() {
		return parsedVersion;
	}

	public String getGroupId() {
		return projectId.getGroupId();
	}

	public String getArtifactId() {
		return projectId.getArtifactId();
	}

	public String getPackaging() {
		return projectId.getPackaging();
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MavenVersionedProjectId other = (MavenVersionedProjectId) obj;
		return projectId.equals(other.projectId) && Objects.equals(version, other.version);
	}

	@Override
	public String toString() {
		return String.format("%s:%s", projectId, version);
	}

	/**
	 * The relative path if this project inside a repository
	 */
	public String repositoryPath() {
		return projectId.repositoryPath() + "/" + version;
	}

	/**
	 * The relative path if this project POM inside a repository
	 */
	public String repositoryPom() {
		return repositoryPath() + "/" + projectId.getArtifactId() + "-" + version + ".pom";
	}

	public static MavenVersionedProjectId parse(String input) {
		String[] parts = input.split(":");
		if (parts.length != 4) {
			throw new IllegalArgumentException(String.format(
					"Invalid project id %s, expecting 4 parts, but only has %d : %s",
					input,
					parts.length,
					Arrays.toString(parts)));
		}
		return new MavenVersionedProjectId(new MavenProjectId(parts[0], parts[1], parts[2]), parts[3]);
	}

	public static MavenVersionedProjectId from(MavenProject project) {
		Objects.requireNonNull(project, "project is null");
		return new MavenVersionedProjectId(MavenProjectId.from(project),
				project.getVersion());
	}

	public static MavenVersionedProjectId from(Dependency mavenDependency) {
		return new MavenVersionedProjectId(MavenProjectId.from(mavenDependency), mavenDependency.getVersion());
	}

}
