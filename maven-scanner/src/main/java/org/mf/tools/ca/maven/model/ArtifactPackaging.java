package org.mf.tools.ca.maven.model;

public enum ArtifactPackaging {
	UNKNOWN,
	JAR,
	POM,
	WAR;
	
	public static ArtifactPackaging resolve(String type) {
		switch (type) {
		case "pom":
			return ArtifactPackaging.POM;
		case "jar":
			return ArtifactPackaging.JAR;
		case "war":
			return ArtifactPackaging.WAR;
		default:
			return ArtifactPackaging.UNKNOWN;
		}
	}
}
