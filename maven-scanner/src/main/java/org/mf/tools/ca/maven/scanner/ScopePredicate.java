package org.mf.tools.ca.maven.scanner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

import org.mf.tools.ca.maven.model.Scope;

/**
 * @author Marc Flament
 * @created 2018-11-04
 */
public class ScopePredicate implements Predicate<Scope> {

	private final Set<Scope> includedScopes;
	private final Set<Scope> excludedScopes;

	private ScopePredicate(Builder builder) {
		this.includedScopes = builder.includedScopes.isEmpty() ? null : builder.includedScopes;
		this.excludedScopes = builder.excludedScopes.isEmpty() ? null : builder.excludedScopes;
	}

	@Override
	public boolean test(Scope scope) {
		if (excludedScopes != null && excludedScopes.contains(scope))
			return false;
		if (includedScopes != null && !includedScopes.contains(scope))
			return false;
		return true;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private final Set<Scope> includedScopes = new HashSet<>();
		private final Set<Scope> excludedScopes = new HashSet<>();

		public Builder includeScopes(Scope... scopes) {
			includedScopes.addAll(Arrays.asList(scopes));
			return this;
		}

		public Builder excludeScopes(Scope... scopes) {
			excludedScopes.addAll(Arrays.asList(scopes));
			return this;
		}

		public ScopePredicate build() {
			return new ScopePredicate(this);
		}
	}
}
