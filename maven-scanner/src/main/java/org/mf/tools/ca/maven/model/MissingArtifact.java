package org.mf.tools.ca.maven.model;

import java.util.Collections;
import java.util.Set;

/**
 * @author Marc Flament
 * @created 2018-11-04
 */
public class MissingArtifact extends AbstractMavenArtifact {

	public MissingArtifact(MavenArtifactId artifactId) {
		super(artifactId, null);
	}

	@Override
	public Set<MavenDependency> getDependencies() {
		return Collections.emptySet();
	}

	@Override
	public void addDependency(MavenDependency dependency) {
		throw new UnsupportedOperationException("Can not add dependencies to a missing artifact");
	}

	@Override
	public String toString() {
		return super.toString() + " (missing)";
	}
}
