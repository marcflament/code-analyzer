package org.mf.tools.ca.maven.model;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

/**
 * @author Marc Flament
 * @created 2018-10-27
 */
public abstract class AbstractMavenArtifact implements MavenArtifact {

	protected final MavenArtifactId artifactId;

	protected final Set<MavenDependency> dependencies;

	public AbstractMavenArtifact(MavenArtifactId artifactId, Set<MavenDependency> dependencies) {
		super();
		this.artifactId = Objects.requireNonNull(artifactId, "artifactId is null");
		this.dependencies = dependencies;
	}

	@Override
	public boolean wasResolved() {
		return dependencies != null;
	}

	@Override
	public MavenArtifactId getId() {
		return artifactId;
	}

	@Override
	public Set<MavenDependency> getDependencies() {
		return Collections.unmodifiableSet(dependencies);
	}

	@Override
	public void addDependency(MavenDependency dependency) {
		this.dependencies.add(dependency);
	}

	@Override
	public int hashCode() {
		return artifactId.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MavenArtifact))
			return false;
		MavenArtifact other = (MavenArtifact) obj;
		return artifactId.equals(other.getId());
	}

	@Override
	public String toString() {
		return artifactId.toString();
	}

}
