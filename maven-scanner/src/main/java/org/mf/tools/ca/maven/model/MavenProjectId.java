package org.mf.tools.ca.maven.model;

import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.maven.model.Dependency;
import org.apache.maven.project.MavenProject;

public final class MavenProjectId {

	private static final String DEFAULT_PACKAGING = "jar";

	private final String groupId;
	private final String artifactId;
	private final String packaging;

	private final int hashCode;

	public MavenProjectId(String groupId, String artifactId, String packaging) {
		super();
		this.groupId = groupId;
		this.artifactId = artifactId;
		this.packaging = packaging == null ? DEFAULT_PACKAGING : packaging;
		this.hashCode = new HashCodeBuilder().append(groupId).append(artifactId).toHashCode();
	}

	public String getGroupId() {
		return groupId;
	}

	public String getArtifactId() {
		return artifactId;
	}

	public String getPackaging() {
		return packaging;
	}

	public ArtifactPackaging resolvePackaging() {
		return ArtifactPackaging.resolve(packaging);
	}

	@Override
	public String toString() {
		return String.format("%s:%s:%s", groupId, artifactId, packaging);
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MavenProjectId other = (MavenProjectId) obj;
		return new EqualsBuilder().append(groupId, other.groupId)
				.append(artifactId, other.artifactId)
				.isEquals();
	}

	/**
	 * The relative path if this project inside a repository
	 */
	public String repositoryPath() {
		return groupId.replace('.', '/') + '/' + artifactId;
	}

	public static MavenProjectId from(MavenProject project) {
		Objects.requireNonNull(project, "project is null");
		return new MavenProjectId(project.getGroupId(),
				project.getArtifactId(),
				project.getPackaging());
	}

	/**
	 * groupId:artifactId:packaging:version
	 */
	public static MavenProjectId parse(String id) {
		String[] parts = id.split(":");
		if (parts.length != 3) {
			throw new IllegalArgumentException(String.format(
					"Invalid project id %s, expecting 3 parts, but only has %d : %s",
					id,
					parts.length,
					Arrays.toString(parts)));
		}
		return new MavenProjectId(parts[0], parts[1], parts[2]);
	}

	public static MavenProjectId from(Dependency mavenDependency) {
		return new MavenProjectId(mavenDependency.getGroupId(),
				mavenDependency.getArtifactId(),
				mavenDependency.getType());
	}

}