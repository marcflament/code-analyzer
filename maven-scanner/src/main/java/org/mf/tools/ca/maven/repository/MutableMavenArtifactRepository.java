package org.mf.tools.ca.maven.repository;

import java.util.function.Function;

import org.mf.tools.ca.maven.model.MavenArtifact;
import org.mf.tools.ca.maven.model.MavenArtifactId;

public interface MutableMavenArtifactRepository extends MavenArtifactRepository {

	MavenArtifact put(MavenArtifact artifact);

	MavenArtifact computeIfAbsent(MavenArtifactId artifactId, Function<MavenArtifactId, MavenArtifact> factory);

}
