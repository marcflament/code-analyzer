package org.mf.tools.ca.maven.model;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Marc Flament
 * @created 2018-10-28
 */
public class RepositoryArtifact extends AbstractMavenArtifact {

	private final File file;

	private RepositoryArtifact(MavenArtifactId artifactId, File file, Set<MavenDependency> dependencies) {
		super(artifactId, dependencies);
		this.file = Objects.requireNonNull(file, "file is null for artifact " + artifactId);
	}

	public File getFile() {
		return file;
	}

	@Override
	public String toString() {
		return String.format("%s (%s)", super.toString(), file);
	}

	public static RepositoryArtifact resolvableArtifact(MavenArtifactId artifactId, File file) {
		return new RepositoryArtifact(artifactId, file, new LinkedHashSet<>());
	}

	public static RepositoryArtifact unresolvableArtifact(MavenArtifactId artifactId, File file) {
		return new RepositoryArtifact(artifactId, file, null);
	}

}
