package org.mf.tools.ca.maven.scanner.repository;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.apache.maven.model.Parent;
import org.apache.maven.project.MavenProject;
import org.mf.tools.ca.maven.model.MavenVersionedProjectId;
import org.mf.tools.ca.maven.utils.MavenFiles;
import org.mf.tools.ca.maven.utils.MavenProjectLoader;

/**
 * @author Marc Flament
 * @created 2018-11-01
 */
public class MavenProjectRepository {

	@SuppressWarnings("serial")
	private static class ProjectsCache extends LinkedHashMap<File, Optional<MavenProject>> {
		private final int cacheSize;

		public ProjectsCache(int cacheSize) {
			super();
			this.cacheSize = cacheSize;
		}

		@Override
		protected boolean removeEldestEntry(Entry<File, Optional<MavenProject>> eldest) {
			return size() > cacheSize;
		}
	}

	public static final int DEFAULT_CACHE_SIZE = 1000;

	private final ProjectsCache projectsCache;

	private final MavenProjectLoader projectLoader = new MavenProjectLoader();

	private final File localRepository;

	public MavenProjectRepository() {
		this(DEFAULT_CACHE_SIZE);
	}

	public MavenProjectRepository(int cacheSize) {
		this.projectsCache = new ProjectsCache(cacheSize);
		this.localRepository = MavenFiles.localRepository();
	}

	public Optional<MavenProject> getProject(MavenVersionedProjectId id) {
		return getProject(new File(localRepository, id.repositoryPom()));
	}

	public Optional<MavenProject> getProject(File pomFile) {
		return projectsCache.computeIfAbsent(pomFile, this::loadProject);
	}

	private Optional<MavenProject> loadProject(File pomFile) {
		if (!pomFile.exists())
			return Optional.empty();

		MavenProject mavenProject = projectLoader.loadMavenProject(pomFile);
		Parent parent = mavenProject.getModel().getParent();
		if (parent != null) {
			File parentFile = createParentFile(parent);
			MavenProject parentProject = getProject(parentFile)
				.orElseThrow(() -> new NoSuchElementException("Parent project " + parentFile + " not found"));
			mavenProject.setParent(parentProject);
			mavenProject.setParentFile(parentProject.getFile());
		}
		return Optional.of(mavenProject);
	}

	private File createParentFile(Parent parent) {
		String pomPath = parent.getGroupId().replace('.', '/');
		pomPath += '/' + parent.getArtifactId();
		pomPath += '/' + parent.getVersion();
		pomPath += '/' + parent.getArtifactId() + '-' + parent.getVersion() + ".pom";
		return new File(localRepository, pomPath);
	}

}
