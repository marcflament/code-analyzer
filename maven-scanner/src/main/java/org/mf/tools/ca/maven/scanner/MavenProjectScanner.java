package org.mf.tools.ca.maven.scanner;

import org.mf.tools.ca.maven.repository.MavenArtifactRepository;

public interface MavenProjectScanner {

	MavenArtifactRepository scan();
	
}
