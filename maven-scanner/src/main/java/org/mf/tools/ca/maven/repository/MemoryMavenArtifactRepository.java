package org.mf.tools.ca.maven.repository;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.mf.tools.ca.maven.model.MavenArtifact;
import org.mf.tools.ca.maven.model.MavenArtifactId;

public class MemoryMavenArtifactRepository implements MutableMavenArtifactRepository {

	private final Map<MavenArtifactId, MavenArtifact> artifacts = new LinkedHashMap<>();

	@Override
	public boolean contains(MavenArtifactId id) {
		return artifacts.containsKey(id);
	}

	@Override
	public Optional<MavenArtifact> get(MavenArtifactId id) {
		return Optional.ofNullable(artifacts.get(id));
	}

	@Override
	public Iterator<MavenArtifact> iterator() {
		return artifacts.values().iterator();
	}

	@Override
	public MavenArtifact put(MavenArtifact artifact) {
		return artifacts.put(artifact.getId(), artifact);
	}

	@Override
	public MavenArtifact computeIfAbsent(MavenArtifactId artifactId, Function<MavenArtifactId, MavenArtifact> factory) {
		return artifacts.computeIfAbsent(artifactId, factory);
	}

}
