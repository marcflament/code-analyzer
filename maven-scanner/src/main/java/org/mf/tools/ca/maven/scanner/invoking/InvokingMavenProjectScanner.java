package org.mf.tools.ca.maven.scanner.invoking;

import static org.mf.tools.ca.maven.utils.MavenProjectLoader.POM_XML;

import java.io.File;
import java.io.FileFilter;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import org.apache.maven.project.MavenProject;
import org.mf.tools.ca.maven.model.DependenciesContainer;
import org.mf.tools.ca.maven.model.MavenArtifact;
import org.mf.tools.ca.maven.model.MavenArtifactId;
import org.mf.tools.ca.maven.model.MavenDependency;
import org.mf.tools.ca.maven.model.MavenVersionedProjectId;
import org.mf.tools.ca.maven.model.RepositoryArtifact;
import org.mf.tools.ca.maven.model.ScannedArtifact;
import org.mf.tools.ca.maven.model.Scope;
import org.mf.tools.ca.maven.repository.MavenArtifactRepository;
import org.mf.tools.ca.maven.repository.MutableMavenArtifactRepository;
import org.mf.tools.ca.maven.scanner.MavenProjectScanner;
import org.mf.tools.ca.maven.utils.MavenFiles;
import org.mf.tools.ca.maven.utils.MavenInvoker;
import org.mf.tools.ca.maven.utils.MavenInvoker.StringHolder;
import org.mf.tools.ca.maven.utils.MavenProjectLoader;
import org.mf.tools.dot.parser.DotTreeNode;
import org.mf.tools.dot.parser.DotTreeParser;
import org.mf.tools.dot.parser.GraphParsingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MavenProjectScanner using maven Invoker API to delegate the dependency
 * resolution to maven.<br/>
 * 
 * @author Marc Flament
 * @created 2018-10-28
 */
public class InvokingMavenProjectScanner implements MavenProjectScanner {

	private static final Logger LOGGER = LoggerFactory.getLogger(InvokingMavenProjectScanner.class);

	private static final FileFilter DIRECTORY_FILTER = f -> f.isDirectory();

	private final File root;

	private final MutableMavenArtifactRepository repository;

	private final Predicate<MavenVersionedProjectId> projectPredicate;

	private final MavenProjectLoader projectLoader = new MavenProjectLoader();

	private final File localRepostiory;

	public InvokingMavenProjectScanner(MutableMavenArtifactRepository repository,
			File root,
			Predicate<MavenVersionedProjectId> projectPredicate) {
		super();
		this.repository = Objects.requireNonNull(repository, "repository is null");
		this.root = Objects.requireNonNull(root, "root is null");
		this.projectPredicate = Objects.requireNonNull(projectPredicate, "projectPredicate is null");
		this.localRepostiory = MavenFiles.localRepository();
	}

	@Override
	public MavenArtifactRepository scan() {
		collectPoms().stream()
				.map(projectLoader::loadMavenProject)
				.filter(p -> !Objects.equals("pom", p.getPackaging())
						&& projectPredicate.test(MavenVersionedProjectId.from(p)))
				.forEach(this::createProjectArtifact);
		return repository;
	}

	private List<File> collectPoms() {
		List<File> res = new LinkedList<>();
		collectPoms(root, res);
		return res;
	}

	private static void collectPoms(File file, List<File> results) {
		if (file.isDirectory()) {
			File pomFile = new File(file, POM_XML);
			if (pomFile.exists())
				results.add(pomFile);
			File[] subdirs = file.listFiles(DIRECTORY_FILTER);
			for (int i = 0; i < subdirs.length; i++) {
				collectPoms(subdirs[i], results);
			}
		} else
			throw new IllegalArgumentException(file.getAbsolutePath() + " is not a directory");
	}

	private MavenArtifact createProjectArtifact(MavenProject project) {
		MavenArtifactId artifactId = MavenArtifactId.from(project);
		MavenArtifact artifact = repository.computeIfAbsent(artifactId, id -> new ScannedArtifact(id, project));
		DotTreeNode node = createDependencies(project.getFile());
		if (node != null)
			node.forEach(c -> createDependencies(c, artifact));
		return artifact;
	}

	private void createDependencies(DotTreeNode node, DependenciesContainer container) {
		MavenArtifactId artifactId = MavenDependency.parseArtifactId(node.getName());
		MavenArtifact dependencyArtifact = createDependencyArtifact(artifactId);
		Scope scope = MavenDependency.parseScope(node.getName());
		MavenDependency dependency = new MavenDependency(dependencyArtifact, scope, false);
		container.addDependency(dependency);
		node.forEach(c -> createDependencies(c, dependency));
	}

	private MavenArtifact createDependencyArtifact(MavenArtifactId artifactId) {
		File artifactFile = new File(localRepostiory, artifactId.repositoryFile());
		return RepositoryArtifact.unresolvableArtifact(artifactId, artifactFile);
	}

	private static DotTreeNode createDependencies(File pomFile) {
		StringHolder output = new StringHolder();
		MavenInvoker.forPom(pomFile)
				.goals("dependency:tree")
				.property("scope", "runtime")
				.property("outputType", "dot")
				.textOutput("outputFile", output)
				.invoke();

		if (output.get().trim().length() == 0)
			return null;

		try {
			return DotTreeParser.parse(output.get());
		} catch (GraphParsingException e) {
			LOGGER.warn("Error parsing dependency tree of {}", pomFile, e);
			return null;
		}
	}

	public static Builder builder(MutableMavenArtifactRepository repository, File root) {
		return new Builder(repository, root);
	}

	public static final class Builder {
		private final MutableMavenArtifactRepository repository;
		private final File root;
		private Predicate<MavenVersionedProjectId> projectPredicate;

		public Builder(MutableMavenArtifactRepository repository,
				File root) {
			super();
			this.repository = Objects.requireNonNull(repository, "repository is null");
			this.root = Objects.requireNonNull(root, "root is null");
		}

		public Builder withProjectPredicate(Predicate<MavenVersionedProjectId> projectPredicate) {
			this.projectPredicate = projectPredicate;
			return this;
		}

		public InvokingMavenProjectScanner build() {
			if (projectPredicate == null)
				projectPredicate = id -> true;
			return new InvokingMavenProjectScanner(repository, root, projectPredicate);
		}
	}
}
