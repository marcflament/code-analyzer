package org.mf.tools.ca.maven.repository;

import org.mf.tools.ca.maven.model.DependenciesContainer;

public class MavenArtifactRepositoryUtils {

	private static final String LINE_SEPARATOR = System.lineSeparator();

	private MavenArtifactRepositoryUtils() {}

	public static String print(MavenArtifactRepository repository) {
		StringBuilder sb = new StringBuilder();
		repository.forEach(a -> printDependencies(a, 0, sb));
		return sb.toString();
	}
	
	public static String print(DependenciesContainer container) {
		StringBuilder sb = new StringBuilder();
		printDependencies(container, 0, sb);
		return sb.toString();
	}

	private static void printDependencies(DependenciesContainer container, int depth, StringBuilder sb) {
		for (int i = 0; i < depth; i++)
			sb.append("  ");
		sb.append(container).append(LINE_SEPARATOR);
		container.forEach(d -> printDependencies(d, depth + 1, sb));
	}
}
