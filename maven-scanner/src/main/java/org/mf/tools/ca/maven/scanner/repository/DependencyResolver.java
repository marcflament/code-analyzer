package org.mf.tools.ca.maven.scanner.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Exclusion;
import org.apache.maven.project.MavenProject;
import org.mf.tools.ca.maven.model.MavenVersionedProjectId;
import org.mf.tools.ca.maven.model.Scope;

public class DependencyResolver {

	private static final class DependenciesExclusion implements Predicate<Dependency> {

		private static final String WILDCARD = "*";

		private final String groupId;
		private final String artifactId;

		public DependenciesExclusion(Exclusion exclusion) {
			this.groupId = exclusion.getGroupId();
			this.artifactId = exclusion.getArtifactId();
		}

		@Override
		public boolean test(Dependency input) {
			return (WILDCARD.equals(groupId) || groupId.equals(input.getGroupId())) &&
					(WILDCARD.equals(artifactId) || artifactId.equals(input.getArtifactId()));
		}

	}

	private final class DependencyResolutionContext {

		private final EffectiveMavenProject effectiveProject;

		private final List<ResolvedDependency> dependencies;

		private final Map<String, ResolvedDependency> resolvedDependencies = new LinkedHashMap<>();

		public DependencyResolutionContext(MavenProject project) {
			effectiveProject = effectiveProjectFactory.create(project);
			this.dependencies = createDependencies(effectiveProject);
		}

		private List<ResolvedDependency> createDependencies(EffectiveMavenProject project) {
			List<Dependency> dependencies = project.getDependencies();
			List<ResolvedDependency> res = new ArrayList<>(dependencies.size());
			for (Dependency dependency : dependencies) {
				Dependency mergedDependency = mergeDependency(project, dependency);
				if (acceptScope(mergedDependency) && !isResolved(mergedDependency)) {
					res.add(createDependency(null, mergedDependency));
				}
			}
			return res;
		}

		private Dependency mergeDependency(EffectiveMavenProject project, Dependency dependency) {
			Dependency mergedDependency = project.mergeDependency(dependency.clone());
			if (project != effectiveProject)
				mergedDependency = effectiveProject.mergeDependency(mergedDependency);
			return mergedDependency;
		}

		private ResolvedDependency createDependency(ResolvedDependency parent, Dependency dependency) {
			MavenProject project = getDependencyProject(dependency);
			EffectiveMavenProject effectiveDependencyProject = project == null ? null
					: effectiveProjectFactory.create(project);
			ResolvedDependency res = new ResolvedDependency(parent, dependency, effectiveDependencyProject);
			addResolved(res);
			return res;
		}

		private MavenProject getDependencyProject(Dependency dependency) {
			if (dependency.getGroupId() == null ||
					dependency.getArtifactId() == null ||
					dependency.getVersion() == null)
				return null;
			return projectRepository.getProject(MavenVersionedProjectId.from(dependency)).orElse(null);
		}

		private boolean acceptScope(Dependency dependency) {
			return acceptScope(Scope.resolve(dependency.getScope()));
		}

		private boolean acceptScope(Scope scope) {
			return scopePredicate.test(scope);
		}

		private boolean isResolved(Dependency dependency) {
			return resolvedDependencies.containsKey(dependency.getManagementKey());
		}

		private void addResolved(ResolvedDependency dependency) {
			resolvedDependencies.put(dependency.getManagementKey(), dependency);
		}
	}

	public static final class ResolvedDependency implements Iterable<ResolvedDependency> {

		private final Dependency dependency;

		private final EffectiveMavenProject effectiveDependencyProject;

		private final List<DependenciesExclusion> exclusions;

		private final ResolvedDependency parent;

		private final List<ResolvedDependency> children = new LinkedList<>();

		private final Scope scope;

		private ResolvedDependency(ResolvedDependency parent,
				Dependency dependency,
				EffectiveMavenProject effectiveDependencyProject) {
			this.parent = parent;
			this.dependency = dependency;
			this.effectiveDependencyProject = effectiveDependencyProject;
			this.scope = Scope.resolve(dependency.getScope());
			this.exclusions = createExclusions();
		}

		public Dependency getDependency() {
			return dependency;
		}

		public Scope getScope() {
			return scope;
		}

		public boolean isResolved() {
			return effectiveDependencyProject != null;
		}

		private List<DependenciesExclusion> createExclusions() {
			if (!dependency.getExclusions().isEmpty()) {
				return dependency.getExclusions()
						.stream()
						.map(DependenciesExclusion::new)
						.collect(Collectors.toCollection(() -> new ArrayList<>(dependency.getExclusions().size())));
			}
			return Collections.emptyList();
		}

		@Override
		public Iterator<ResolvedDependency> iterator() {
			return children.iterator();
		}

		private List<ResolvedDependency> createChildren(DependencyResolutionContext context) {
			if (!isResolved())
				return Collections.emptyList();

			List<Dependency> transitiveDependencies = effectiveDependencyProject.getDependencies();
			for (Dependency transitiveDependency : transitiveDependencies) {
				transitiveDependency = context.mergeDependency(effectiveDependencyProject, transitiveDependency);

				if (isExcluded(transitiveDependency)) // excluded dependency
					continue;

				if (context.isResolved(transitiveDependency)) // already declared
					continue;

				if (transitiveDependency.isOptional()) // optional are not imported
					continue;

				Scope dependencyScope = getDependencyScope(transitiveDependency);
				if (dependencyScope == null || !context.acceptScope(dependencyScope))
					continue;

				transitiveDependency.setScope(dependencyScope.toString());
				ResolvedDependency child = context.createDependency(this, transitiveDependency);
				children.add(child);
			}
			return children;
		}

		private Scope getDependencyScope(Dependency transitiveDependency) {
			Scope dependencyScope = Scope.resolve(transitiveDependency.getScope());
			switch (dependencyScope) {
				case COMPILE:
					return scope;
				case RUNTIME:
					return scope == Scope.COMPILE ? Scope.RUNTIME : scope;
				default:
					return null;
			}
		}

		private boolean isExcluded(Dependency dependency) {
			for (DependenciesExclusion exclusion : exclusions) {
				if (exclusion.test(dependency))
					return true;
			}
			if (parent != null)
				return parent.isExcluded(dependency);
			return false;
		}

		public String getManagementKey() {
			return dependency.getManagementKey();
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(dependency.getGroupId())
					.append(":")
					.append(dependency.getArtifactId())
					.append(":")
					.append(dependency.getType());
			if (dependency.getClassifier() != null)
				sb.append(":")
						.append(dependency.getClassifier());

			sb.append(":").append(dependency.getVersion()).append(":").append(scope);
			return sb.toString();
		}

		public boolean hasTransitiveDependencies() {
			return !children.isEmpty();
		}

		public String printDependencyTree() {
			StringBuilder sb = new StringBuilder();
			printDependency(this, 0, sb);
			return sb.toString();
		}

		private static void printDependency(ResolvedDependency dependency, int depth, StringBuilder sb) {
			for (int i = 0; i < depth; i++)
				sb.append(" ");
			sb.append(dependency).append(System.lineSeparator());
			for (ResolvedDependency child : dependency) {
				printDependency(child, depth + 1, sb);
			}
		}

		public boolean isOptional() {
			return dependency.isOptional();
		}
	}

	private final MavenProjectRepository projectRepository;

	private final Predicate<Scope> scopePredicate;

	private final EffectiveMavenProjectFactory effectiveProjectFactory;

	public DependencyResolver(MavenProjectRepository projectRepository, Predicate<Scope> scopePredicate) {
		this.projectRepository = projectRepository;
		this.scopePredicate = scopePredicate;
		this.effectiveProjectFactory = new EffectiveMavenProjectFactory(projectRepository);
	}

	public List<ResolvedDependency> resolve(MavenProject project) {
		DependencyResolutionContext context = new DependencyResolutionContext(project);
		List<ResolvedDependency> currentDependencies = context.dependencies;
		while (!currentDependencies.isEmpty()) {
			List<ResolvedDependency> nextDependencies = new LinkedList<>();
			currentDependencies.stream()
					.filter(ResolvedDependency::isResolved)
					.forEach(d -> nextDependencies.addAll(d.createChildren(context)));
			currentDependencies = nextDependencies;
		}
		return context.dependencies;
	}

}
