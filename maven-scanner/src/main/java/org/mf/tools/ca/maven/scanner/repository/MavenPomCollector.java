package org.mf.tools.ca.maven.scanner.repository;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

/**
 * @author Marc Flament
 * @created 2018-11-01
 */
public class MavenPomCollector {

	private static final String POM_EXTENSION = "pom";

	private static final class PomFile implements Comparable<PomFile> {

		private final File file;

		private final ArtifactVersion version;

		public PomFile(File file) {
			super();
			this.file = file;
			this.version = new DefaultArtifactVersion(file.getParentFile().getName());
		}

		public File getFile() {
			return file;
		}

		@Override
		public int compareTo(PomFile o) {
			return version.compareTo(o.version);
		}

		@Override
		public String toString() {
			return file.toString();
		}

		@Override
		public int hashCode() {
			return file.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PomFile other = (PomFile) obj;
			return file.equals(other.file);
		}

	}

	private static final class ProjectPoms {

		private final List<PomFile> pomFiles = new LinkedList<>();

		public void addPom(File pomFile) {
			this.pomFiles.add(new PomFile(pomFile));
		}

		public File lastPom() {
			PomFile lastPom = null;
			for (PomFile pomFile : pomFiles) {
				if (lastPom == null || pomFile.compareTo(lastPom) > 0)
					lastPom = pomFile;
			}
			return lastPom == null ? null : lastPom.file;
		}

		public List<File> allPoms() {
			return pomFiles.stream()
				.map(PomFile::getFile)
				.collect(Collectors.toCollection(() -> new ArrayList<>(pomFiles.size())));
		}
	}

	private final File baseDirectory;

	private final boolean lastVersionOnly;

	/**
	 * @param repositoryDirectory
	 *            the absolute directory of the maven local repository
	 * @param baseDirectory
	 *            the relative path to scan inside the repositoryDirectory
	 * @param lastVersionOnly
	 *            only collect the last version of maven poms
	 */
	public MavenPomCollector(File baseDirectory, boolean lastVersionOnly) {
		super();
		checkDirectory(baseDirectory);
		this.baseDirectory = Objects.requireNonNull(baseDirectory, "baseDirectory is null");
		this.lastVersionOnly = lastVersionOnly;
	}

	private static void checkDirectory(File directory) {
		if (!directory.exists())
			throw new IllegalArgumentException(directory + " does not exists");
		if (!directory.isDirectory())
			throw new IllegalArgumentException(directory + " is not a directory");
	}

	public Collection<File> collectPoms() {
		Map<File, ProjectPoms> projectDirectories = new LinkedHashMap<>();
		collectPoms(baseDirectory, projectDirectories);
		Collection<File> results = new LinkedList<>();
		projectDirectories.values().stream().forEach(d -> {
			if (lastVersionOnly)
				results.add(d.lastPom());
			else
				results.addAll(d.allPoms());
		});
		return results;
	}

	private void collectPoms(File directory, Map<File, ProjectPoms> projectDirectories) {
		if (directory.isDirectory()) {
			File[] files = directory.listFiles();
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				if (file.isDirectory())
					collectPoms(file, projectDirectories);
				else if (isPomFile(file)) {
					File projectDir = file.getParentFile().getParentFile();
					projectDirectories.computeIfAbsent(projectDir, d -> new ProjectPoms()).addPom(file);
				}
			}
		} else
			throw new IllegalArgumentException(directory + " is not a directory");
	}

	private static boolean isPomFile(File file) {
		return FilenameUtils.getExtension(file.getName()).equals(POM_EXTENSION);
	}

}
