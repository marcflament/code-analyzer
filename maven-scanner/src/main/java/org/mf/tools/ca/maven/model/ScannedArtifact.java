package org.mf.tools.ca.maven.model;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.maven.project.MavenProject;

/**
 * @author Marc Flament
 * @created 2018-10-27
 */
public class ScannedArtifact extends AbstractMavenArtifact {

	protected final MavenProject mavenProject;

	public ScannedArtifact(MavenProject mavenProject) {
		this(MavenArtifactId.from(mavenProject), mavenProject);
	}

	public ScannedArtifact(MavenArtifactId artifactId, MavenProject mavenProject) {
		super(artifactId, new LinkedHashSet<>());
		this.mavenProject = mavenProject;
		Objects.requireNonNull(mavenProject.getBasedir(), "mavenProject.getBasedir() is null");
	}

	public MavenProject getMavenProject() {
		return mavenProject;
	}

	public List<File> getSourceDirectories() {
		return mavenProject.getCompileSourceRoots()
			.stream()
			.map(d -> new File(mavenProject.getBasedir(), d))
			.filter(File::exists)
			.collect(Collectors.toList());
	}

}
