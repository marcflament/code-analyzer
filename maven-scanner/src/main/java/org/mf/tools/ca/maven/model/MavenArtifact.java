package org.mf.tools.ca.maven.model;

/**
 * @author Marc Flament
 * @created 2018-10-27
 */
public interface MavenArtifact extends DependenciesContainer {

	MavenArtifactId getId();

	/** 
	 * @return true if the dependencies resolution was attempted (getDependencies() will not be null)
	 * false if no dependency resolution was done (getDependencies() will be null)
	 */
	boolean wasResolved();
	
	default String getPackaging() {
		return getId().getPackaging();
	}

	default String getGroupId() {
		return getId().getGroupId();
	}

	default String getArtifactId() {
		return getId().getArtifactId();
	}

	default String getVersion() {
		return getId().getVersion();
	}

	default String getClassifier() {
		return getId().getClassifier();
	}

	default boolean hasClassifier() {
		return getId().hasClassifier();
	}

}
