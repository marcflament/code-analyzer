package org.mf.tools.ca.maven.scanner.repository;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.function.Predicate;

import org.apache.maven.project.MavenProject;
import org.mf.tools.ca.maven.model.DependenciesContainer;
import org.mf.tools.ca.maven.model.MavenArtifact;
import org.mf.tools.ca.maven.model.MavenArtifactId;
import org.mf.tools.ca.maven.model.MavenDependency;
import org.mf.tools.ca.maven.model.MavenVersionedProjectId;
import org.mf.tools.ca.maven.model.MissingArtifact;
import org.mf.tools.ca.maven.model.RepositoryArtifact;
import org.mf.tools.ca.maven.model.Scope;
import org.mf.tools.ca.maven.repository.MavenArtifactRepository;
import org.mf.tools.ca.maven.repository.MutableMavenArtifactRepository;
import org.mf.tools.ca.maven.scanner.MavenProjectScanner;
import org.mf.tools.ca.maven.scanner.ScopePredicate;
import org.mf.tools.ca.maven.scanner.repository.DependencyResolver.ResolvedDependency;
import org.mf.tools.ca.maven.utils.MavenFiles;

/**
 * A MavenProjectScanner that scan the local repository and use local artifacts
 * and pom analysis to create dependencies resolution.
 * 
 * @author Marc Flament
 * @created 2018-10-28
 */
public class RepositoryMavenProjectScanner implements MavenProjectScanner {

	private final class DependenciesFactoryTask {
		private final DependenciesContainer container;
		private final Iterable<ResolvedDependency> dependencies;

		public DependenciesFactoryTask(DependenciesContainer container,
				Iterable<ResolvedDependency> dependencies) {
			super();
			this.container = container;
			this.dependencies = dependencies;
		}

		public void run() {
			for (ResolvedDependency resolvedDependency : dependencies) {
				MavenArtifact artifact;
				MavenArtifactId artifactId = MavenArtifactId.from(resolvedDependency.getDependency());
				if (resolvedDependency.isResolved())
					artifact = createArtifact(artifactId, recursiveResolve);
				else
					artifact = createMissingArtifact(artifactId);
				MavenDependency dependency = new MavenDependency(artifact,
						resolvedDependency.getScope(),
						resolvedDependency.isOptional());
				container.addDependency(dependency);
				if (resolvedDependency.hasTransitiveDependencies())
					pendingTasks.offer(new DependenciesFactoryTask(dependency, resolvedDependency));
			}
		}
	}

	private final MutableMavenArtifactRepository repository;

	private final boolean recursiveResolve;

	private final File localRepository;

	private final MavenPomCollector pomCollector;

	private final Predicate<MavenVersionedProjectId> projectPredicate;

	private final MavenProjectRepository projectRepository;

	private final DependencyResolver dependencyResolver;

	private final Queue<DependenciesFactoryTask> pendingTasks = new LinkedList<>();

	private RepositoryMavenProjectScanner(MutableMavenArtifactRepository repository,
			boolean recursiveResolve,
			File localRepository,
			MavenPomCollector pomCollector,
			Predicate<MavenVersionedProjectId> projectPredicate,
			MavenProjectRepository projectRepository,
			Predicate<Scope> scopePredicate) {
		this.repository = Objects.requireNonNull(repository, "repository is null");
		this.localRepository = Objects.requireNonNull(localRepository, "localRepository is null");
		this.pomCollector = Objects.requireNonNull(pomCollector, "pomCollector is null");
		this.projectRepository = Objects.requireNonNull(projectRepository, "projectRepository is null");
		this.projectPredicate = Objects.requireNonNull(projectPredicate, "projectPredicate is null");
		this.dependencyResolver = new DependencyResolver(projectRepository,
				Objects.requireNonNull(scopePredicate, "scopePredicate is null"));
		this.recursiveResolve = recursiveResolve;
	}

	@Override
	public MavenArtifactRepository scan() {
		pomCollector.collectPoms()
				.stream()
				.map(f -> projectRepository.getProject(f)
						.orElseThrow(() -> new IllegalStateException("No project found for " + f)))
				.map(MavenArtifactId::from)
				.filter(aid -> projectPredicate.test(aid.getVersionedProjectId()))
				.forEach(aid -> createArtifact(aid, true));

		DependenciesFactoryTask task;
		while ((task = pendingTasks.poll()) != null) {
			task.run();
		}
		return repository;
	}

	private MavenArtifact createArtifact(MavenArtifactId artifactId, boolean recurse) {
		return repository.computeIfAbsent(artifactId, id -> {
			File artifactFile = new File(localRepository, artifactId.repositoryFile());
			RepositoryArtifact artifact = recurse ? RepositoryArtifact.resolvableArtifact(id, artifactFile)
					: RepositoryArtifact.unresolvableArtifact(id, artifactFile);
			MavenProject project = projectRepository.getProject(artifact.getId().getVersionedProjectId())
					.orElseThrow(() -> new IllegalStateException("No project found for " + artifact.getId()));
			if (recurse) {
				List<ResolvedDependency> dependencies = dependencyResolver.resolve(project);
				if (!dependencies.isEmpty())
					pendingTasks.add(new DependenciesFactoryTask(artifact, dependencies));
			}
			return artifact;
		});
	}

	private MavenArtifact createMissingArtifact(MavenArtifactId artifactId) {
		return repository.computeIfAbsent(artifactId, MissingArtifact::new);
	}

	public static Builder builder(MutableMavenArtifactRepository repository) {
		return new Builder(repository);
	}

	public static final class Builder {
		private final MutableMavenArtifactRepository repository;

		private File localRepository;
		private boolean recursiveResolve;
		private MavenPomCollector pomCollector;
		private Predicate<MavenVersionedProjectId> projectPredicate;
		private MavenProjectRepository projectRepository;
		private Predicate<Scope> scopePredicate;

		public Builder(MutableMavenArtifactRepository repository) {
			super();
			this.repository = Objects.requireNonNull(repository, "repository is null");
		}

		public Builder withLocalRepository(File localReposiotry) {
			this.localRepository = localReposiotry;
			return this;
		}

		public Builder withRecursiveResolve(boolean recursiveResolve) {
			this.recursiveResolve = recursiveResolve;
			return this;
		}

		public Builder withPomCollector(MavenPomCollector pomCollector) {
			this.pomCollector = pomCollector;
			return this;
		}

		public Builder withProjectPredicate(Predicate<MavenVersionedProjectId> projectPredicate) {
			this.projectPredicate = projectPredicate;
			return this;
		}

		public Builder withProjectRepository(MavenProjectRepository projectRepository) {
			this.projectRepository = projectRepository;
			return this;
		}

		public Builder withScopePredicate(Predicate<Scope> scopePredicate) {
			this.scopePredicate = scopePredicate;
			return this;
		}

		public RepositoryMavenProjectScanner build() {
			if (localRepository == null)
				localRepository = MavenFiles.localRepository();
			if (pomCollector == null)
				pomCollector = new MavenPomCollector(localRepository, true);
			if (projectPredicate == null)
				projectPredicate = d -> true;
			if (projectRepository == null)
				projectRepository = new MavenProjectRepository();
			if (scopePredicate == null)
				scopePredicate = ScopePredicate.builder().excludeScopes(Scope.TEST).build();

			return new RepositoryMavenProjectScanner(repository,
					recursiveResolve,
					localRepository,
					pomCollector,
					projectPredicate,
					projectRepository,
					scopePredicate);
		}
	}

}
