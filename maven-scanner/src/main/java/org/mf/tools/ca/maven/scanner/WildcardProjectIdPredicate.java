package org.mf.tools.ca.maven.scanner;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.mf.tools.ca.maven.model.MavenVersionedProjectId;

public class WildcardProjectIdPredicate implements Predicate<MavenVersionedProjectId> {

	private static final Predicate<String> ANY_STRING_PREDICATE = s -> true;

	private static final class PatternPredicate implements Predicate<String> {
		private final Pattern pattern;

		public PatternPredicate(String regex) {
			super();
			this.pattern = Pattern.compile(regex);
		}

		@Override
		public boolean test(String t) {
			return pattern.matcher(t).matches();
		}

	}

	private static final CharSequence WILDCARD_CHARACTER = "*";

	private final Predicate<String> groupIdPredicate;
	private final Predicate<String> artifactIdPredicate;
	private final Predicate<String> versionPredicate;
	private final Predicate<String> packaginPredicate;

	private WildcardProjectIdPredicate(Predicate<String> groupIdPredicate, Predicate<String> artifactIdPredicate,
			Predicate<String> versionPredicate, Predicate<String> packaginPredicate) {
		super();
		this.groupIdPredicate = groupIdPredicate;
		this.artifactIdPredicate = artifactIdPredicate;
		this.versionPredicate = versionPredicate;
		this.packaginPredicate = packaginPredicate;
	}

	@Override
	public boolean test(MavenVersionedProjectId id) {
		return groupIdPredicate.test(id.getGroupId()) &&
				artifactIdPredicate.test(id.getArtifactId()) &&
				versionPredicate.test(id.getVersion()) &&
				packaginPredicate.test(id.getPackaging());
	}

	public static Predicate<MavenVersionedProjectId> parse(String input) {
		if (StringUtils.isBlank(input))
			throw new IllegalArgumentException("input is blank");

		String[] parts = input.split(":");
		int index = 0;
		Predicate<String> groupIdPredicate = parsePredcicate(parts, index++);
		Predicate<String> artifactIdPredicate = parsePredcicate(parts, index++);
		Predicate<String> packaginPredicate = parsePredcicate(parts, index++);
		Predicate<String> versionPredicate = parsePredcicate(parts, index++);
		return new WildcardProjectIdPredicate(groupIdPredicate,
				artifactIdPredicate,
				versionPredicate,
				packaginPredicate);
	}

	private static Predicate<String> parsePredcicate(String[] parts, int index) {
		if (index >= parts.length)
			return ANY_STRING_PREDICATE;

		String part = parts[index];
		if (part.length() == 0)
			return ANY_STRING_PREDICATE;

		if (part.contains(WILDCARD_CHARACTER))
			return new PatternPredicate(part.replaceAll("\\*", ".*"));
		return s -> s.equals(part);
	}

}
