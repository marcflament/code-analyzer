package org.mf.tools.ca.maven.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class MavenDependencyTest {

	@Test
	public void parse_without_classifier() {
		String input = "org.slf4j:slf4j-api:jar:1.7.25:compile";
		MavenArtifactId artifactId = MavenDependency.parseArtifactId(input);
		assertEquals("org.slf4j", artifactId.getGroupId());
		assertEquals("slf4j-api", artifactId.getArtifactId());
		assertEquals("jar", artifactId.getPackaging());
		assertEquals("1.7.25", artifactId.getVersion());
		assertNull(artifactId.getClassifier());
		assertEquals(Scope.COMPILE, MavenDependency.parseScope(input));
	}

	public void parse_with_classifier() {
		String input = "org.slf4j:slf4j-api:jar:sources:1.7.25:compile";
		MavenArtifactId artifactId = MavenDependency.parseArtifactId(input);
		assertEquals("org.slf4j", artifactId.getGroupId());
		assertEquals("slf4j-api", artifactId.getArtifactId());
		assertEquals("jar", artifactId.getPackaging());
		assertEquals("1.7.25", artifactId.getVersion());
		assertEquals("sources", artifactId.getClassifier());
		assertEquals(Scope.COMPILE, MavenDependency.parseScope(input));
	}

	@Test(expected = IllegalArgumentException.class)
	public void parse_error_invalid_scope() {
		MavenDependency.parseScope("org.slf4j:slf4j-api:jar:sources:1.7.25:notascope");
	}
}
