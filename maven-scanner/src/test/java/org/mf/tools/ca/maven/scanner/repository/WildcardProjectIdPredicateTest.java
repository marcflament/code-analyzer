package org.mf.tools.ca.maven.scanner.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.function.Predicate;

import org.junit.Test;
import org.mf.tools.ca.maven.model.MavenVersionedProjectId;
import org.mf.tools.ca.maven.scanner.WildcardProjectIdPredicate;

public class WildcardProjectIdPredicateTest {


	@Test
	public void test() {
		Predicate<MavenVersionedProjectId> predicate;

		predicate = WildcardProjectIdPredicate.parse("*");
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.tools:artifact:jar:0.0.1-SNAPSHOT")));
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.test:artifact:jar:0.0.1-SNAPSHOT")));

		predicate = WildcardProjectIdPredicate.parse("org.mf.tools");
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.tools:artifact:jar:0.0.1-SNAPSHOT")));
		assertFalse(predicate.test(MavenVersionedProjectId.parse("org.mf.test:artifact:jar:0.0.1-SNAPSHOT")));
		
		predicate = WildcardProjectIdPredicate.parse("org.mf.*");
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.tools:artifact:jar:0.0.1-SNAPSHOT")));
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.test:artifact:jar:0.0.1-SNAPSHOT")));
		
		predicate = WildcardProjectIdPredicate.parse("org.mf.tools:artifact");
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.tools:artifact:jar:0.0.1-SNAPSHOT")));
		assertFalse(predicate.test(MavenVersionedProjectId.parse("org.mf.test:artifact:jar:0.0.1-SNAPSHOT")));

		predicate = WildcardProjectIdPredicate.parse("*:artifact");
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.tools:artifact:jar:0.0.1-SNAPSHOT")));
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.test:artifact:jar:0.0.1-SNAPSHOT")));

		predicate = WildcardProjectIdPredicate.parse("::jar:");
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.tools:artifact:jar:0.0.1-SNAPSHOT")));
		assertFalse(predicate.test(MavenVersionedProjectId.parse("org.mf.test:artifact:pom:0.0.1-SNAPSHOT")));

		predicate = WildcardProjectIdPredicate.parse(":::*-SNAPSHOT");
		assertTrue(predicate.test(MavenVersionedProjectId.parse("org.mf.tools:artifact:jar:0.0.1-SNAPSHOT")));
		assertFalse(predicate.test(MavenVersionedProjectId.parse("org.mf.test:artifact:pom:0.0.1")));

	}
}
