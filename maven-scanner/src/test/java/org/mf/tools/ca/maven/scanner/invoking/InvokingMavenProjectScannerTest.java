package org.mf.tools.ca.maven.scanner.invoking;

import org.junit.Test;
import org.mf.tools.ca.maven.repository.MavenArtifactRepository;
import org.mf.tools.ca.maven.repository.MavenArtifactRepositoryUtils;
import org.mf.tools.ca.maven.repository.MemoryMavenArtifactRepository;
import org.mf.tools.ca.maven.scanner.AbstractMavenProjectScannerTest;
import org.mf.tools.ca.maven.scanner.WildcardProjectIdPredicate;

public class InvokingMavenProjectScannerTest extends AbstractMavenProjectScannerTest {

	@Test
	public void test() {
		InvokingMavenProjectScanner scanner = InvokingMavenProjectScanner.builder(new MemoryMavenArtifactRepository(), PROJECTS_DIR)
				.withProjectPredicate(WildcardProjectIdPredicate.parse("sandbox.ca.test*")).build();
		MavenArtifactRepository repository = testScanner(scanner);
		System.out.println(MavenArtifactRepositoryUtils.print(repository));
	}

}
