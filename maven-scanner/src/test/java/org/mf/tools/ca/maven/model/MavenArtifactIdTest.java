package org.mf.tools.ca.maven.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class MavenArtifactIdTest {

	@Test
	public void parse_without_classifier() {
		MavenArtifactId artifactId = MavenArtifactId.parse("org.slf4j:slf4j-api:jar:1.7.25");
		assertEquals("org.slf4j", artifactId.getGroupId());
		assertEquals("slf4j-api", artifactId.getArtifactId());
		assertEquals("jar", artifactId.getPackaging());
		assertEquals("1.7.25", artifactId.getVersion());
		assertNull(artifactId.getClassifier());
	}

	public void parse_with_classifier() {
		MavenArtifactId artifactId = MavenArtifactId.parse("org.slf4j:slf4j-api:jar:sources:1.7.25");
		assertEquals("org.slf4j", artifactId.getGroupId());
		assertEquals("slf4j-api", artifactId.getArtifactId());
		assertEquals("jar", artifactId.getPackaging());
		assertEquals("1.7.25", artifactId.getVersion());
		assertEquals("sources", artifactId.getClassifier());
	}

	@Test(expected = IllegalArgumentException.class)
	public void parse_error_missing_parts() {
		MavenArtifactId.parse("org.slf4j:slf4j-api:1.7.25");
	}
	

	@Test(expected = IllegalArgumentException.class)
	public void parse_error_too_many_parts() {
		MavenArtifactId.parse("org.slf4j:slf4j-api:jar:sources:1.7.25:compile");
	}
}
