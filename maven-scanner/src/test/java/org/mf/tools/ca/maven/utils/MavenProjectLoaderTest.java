package org.mf.tools.ca.maven.utils;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Test;

public class MavenProjectLoaderTest {
	
	private MavenProjectLoader loader;

	@Before
	public void setUp() {
		loader = new MavenProjectLoader();
	}

	@Test
	public void testLoadMavenProjectFile() {
		File file = new File("pom.xml");
		MavenProject mavenProject = loader.loadMavenProject(file);
		assertEquals(file, mavenProject.getFile());
		assertEquals(file.getParentFile(), mavenProject.getBasedir());
		assertEquals("maven-scanner", mavenProject.getArtifactId());
	}

}
