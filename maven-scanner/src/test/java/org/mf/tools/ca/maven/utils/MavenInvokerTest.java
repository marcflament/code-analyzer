package org.mf.tools.ca.maven.utils;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;

public class MavenInvokerTest {

	@Test
	public void test() {
		MavenInvoker.StringHolder output = new MavenInvoker.StringHolder();
		MavenInvoker.forProject(new File("."))
			.goals("help:evaluate")
			.property("expression", "project.artifactId")
			.textOutput("output", output)
			.invoke();
		assertEquals("maven-scanner", output.get().trim());
	}

}
