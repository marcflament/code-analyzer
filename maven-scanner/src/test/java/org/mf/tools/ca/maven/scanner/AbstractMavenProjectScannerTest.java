package org.mf.tools.ca.maven.scanner;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.stream.StreamSupport;

import org.mf.tools.ca.maven.model.MavenArtifactId;
import org.mf.tools.ca.maven.repository.MavenArtifactRepository;
import org.mf.tools.ca.maven.utils.MavenInvoker;

public abstract class AbstractMavenProjectScannerTest {

	protected static final File PROJECTS_DIR = new File("test-projects");

	protected static void installTestProjects() {
		installTestProject("test-project01");
		installTestProject("test-project02");
		installTestProject("test-project03");
	}

	protected static void installSpringBootProject() {
		installTestProject("test-spring-project");
	}

	private static void installTestProject(String project) {
		System.out.println("Installing " + project);
		MavenInvoker.forPom(new File(PROJECTS_DIR, project)).goals("clean", "install").invoke();
	}

	protected MavenArtifactRepository testScanner(MavenProjectScanner scanner) {
		MavenArtifactRepository repository = scan(scanner);

		MavenArtifactId artifactId = MavenArtifactId.parse("sandbox.ca.test.project1:module1:jar:1.0.0");
		assertTrue(repository.get(artifactId).isPresent());

		artifactId = MavenArtifactId.parse("sandbox.ca.test.project2:module1:jar:1.0.0");
		assertTrue(repository.get(artifactId).isPresent());

		artifactId = MavenArtifactId.parse("sandbox.ca.test.project3:module1:jar:1.0.0");
		assertTrue(repository.get(artifactId).isPresent());

		return repository;
	}

	protected MavenArtifactRepository scan(MavenProjectScanner scanner) {
		long startTime = System.currentTimeMillis();
		MavenArtifactRepository repository = scanner.scan();
		long elapsed = System.currentTimeMillis() - startTime;
		System.out.println("Scanned " + StreamSupport.stream(repository.spliterator(), false).count()
				+ " artifacts using " + scanner.getClass().getSimpleName() + " in " + elapsed + "ms");
		return repository;
	}
}
