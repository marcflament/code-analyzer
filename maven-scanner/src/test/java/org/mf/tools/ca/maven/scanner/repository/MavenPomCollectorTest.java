package org.mf.tools.ca.maven.scanner.repository;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.mf.tools.ca.utils.io.TemDirectoryProvider;

import net.lingala.zip4j.core.ZipFile;

public class MavenPomCollectorTest {

	private File baseDirectory;

	@Before
	public void setUp() throws Exception {
		ZipFile zipFile = new ZipFile("test-m2-repo.zip");
		File tempDirectory = TemDirectoryProvider.createTempDirectory("test-");
		zipFile.extractAll(tempDirectory.getAbsolutePath());
		baseDirectory = new File(tempDirectory, "test-m2-repo/org/springframework/spring-core");
	}

	@Test
	public void test_all_versions() {
		MavenPomCollector collector = new MavenPomCollector(baseDirectory, false);
		Collection<File> poms = collector.collectPoms();
		assertEquals(3, poms.size());
	}

	@Test
	public void test_last_versions() {
		MavenPomCollector collector = new MavenPomCollector(baseDirectory, true);
		Collection<File> poms = collector.collectPoms();
		assertEquals(1, poms.size());
		assertEquals("spring-core-5.0.10.RELEASE.pom", poms.iterator().next().getName());
	}

}
