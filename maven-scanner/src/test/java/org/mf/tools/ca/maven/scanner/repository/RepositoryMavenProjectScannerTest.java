package org.mf.tools.ca.maven.scanner.repository;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mf.tools.ca.maven.model.MavenArtifactId;
import org.mf.tools.ca.maven.repository.MavenArtifactRepository;
import org.mf.tools.ca.maven.repository.MavenArtifactRepositoryUtils;
import org.mf.tools.ca.maven.repository.MemoryMavenArtifactRepository;
import org.mf.tools.ca.maven.scanner.AbstractMavenProjectScannerTest;
import org.mf.tools.ca.maven.scanner.MavenProjectScanner;
import org.mf.tools.ca.maven.utils.MavenFiles;

public class RepositoryMavenProjectScannerTest extends AbstractMavenProjectScannerTest {

	@BeforeClass
	public static void setUp() throws Exception {
//		installTestProjects();
//		installSpringBootProject();
	}

	@Test
	public void test_simple() {
		MavenPomCollector pomCollector = new MavenPomCollector(new File(MavenFiles.localRepository(),
				"sandbox/ca/test"),
				true);
		MavenProjectScanner scanner = RepositoryMavenProjectScanner.builder(new MemoryMavenArtifactRepository())
			.withPomCollector(pomCollector)
			.build();
		MavenArtifactRepository repository = testScanner(scanner);
		System.out.println(MavenArtifactRepositoryUtils.print(repository));
	}

	@Test
	public void test_spring() {
		MavenPomCollector pomCollector = new MavenPomCollector(new File(MavenFiles.localRepository(),
				"sandbox/ca/spring"),
				true);

		MavenProjectScanner scanner = RepositoryMavenProjectScanner.builder(new MemoryMavenArtifactRepository())
			.withPomCollector(pomCollector)
			.build();

		MavenArtifactRepository repository = scan(scanner);
		MavenArtifactId artifactId = MavenArtifactId.parse("sandbox.ca.spring:test-spring-project:jar:1.0.0");
		assertTrue(repository.contains(artifactId));
		System.out.println(MavenArtifactRepositoryUtils.print(repository.get(artifactId).get()));

		artifactId = MavenArtifactId.parse("ch.qos.logback:logback-classic:jar:1.2.3");
		assertTrue(repository.contains(artifactId));
		System.out.println(MavenArtifactRepositoryUtils.print(repository.get(artifactId).get()));
	}

	@Test
	public void test_full_scan() {
		Assume.assumeTrue(Boolean.parseBoolean(System.getProperty("test.full")));
		MavenProjectScanner scanner = RepositoryMavenProjectScanner.builder(new MemoryMavenArtifactRepository())
			.build();
		MavenArtifactRepository repository = scan(scanner);
		System.out.println(MavenArtifactRepositoryUtils.print(repository));
	}
	
	@Test
	public void test_boardnox() {
		MavenPomCollector pomCollector = new MavenPomCollector(new File(MavenFiles.localRepository(),
				"com/oodrive/boardnox"),
				true);

		MavenProjectScanner scanner = RepositoryMavenProjectScanner.builder(new MemoryMavenArtifactRepository())
			.withPomCollector(pomCollector)
			.build();

		MavenArtifactRepository repository = scan(scanner);
		MavenArtifactId artifactId = MavenArtifactId.parse("com.oodrive.boardnox:boardnox-web-app:jar:7.6.0-SNAPSHOT");
		assertTrue(repository.contains(artifactId));
		System.out.println(MavenArtifactRepositoryUtils.print(repository.get(artifactId).get()));
	}

}
