package org.mf.tools.dot.parser;

import java.util.HashMap;
import java.util.Map;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.mf.tools.dot.parser.DotParser.GraphContext;

public class DotTreeParser {

	private DotTreeParser() {}

	public static final DotTreeNode parse(String input) throws GraphParsingException {
		DotLexer lexer = new DotLexer(CharStreams.fromString(input));
		DotParser parser = new DotParser(new CommonTokenStream(lexer));
		GraphContext graph = parser.graph();
		if (graph.exception != null)
			throw new GraphParsingException(input, graph.exception);
		DotTreeNode root = new DotTreeNode(name(graph.ID()));
		Map<String, DotTreeNode> nodes = new HashMap<>();
		nodes.put(root.getName(), root);
		graph.statement().stream().forEach(s -> {
			String left = name(s.ID(0));
			String right = name(s.ID(1));
			DotTreeNode leftNode = nodes.get(left);
			if (leftNode == null)
				throw new IllegalArgumentException("Node " + left + " is undefined");
			DotTreeNode rightNode = nodes.computeIfAbsent(right, DotTreeNode::new);
			leftNode.add(rightNode);
		});
		return root;
	}

	private static final String name(TerminalNode idNode) {
		String text = idNode.getText();
		return text.substring(1, text.length() - 1);
	}
}
