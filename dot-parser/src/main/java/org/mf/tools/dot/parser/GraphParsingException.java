package org.mf.tools.dot.parser;

import org.antlr.v4.runtime.RecognitionException;

public class GraphParsingException extends Exception {

	private static final long serialVersionUID = 1L;

	public GraphParsingException(String input, RecognitionException cause) {
		super("Error parsing graph: " + input, cause);
	}

}
