package org.mf.tools.dot.parser;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

public final class DotTreeNode implements Iterable<DotTreeNode> {

	private final String name;

	private final List<DotTreeNode> children = new LinkedList<>();

	public DotTreeNode(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public Iterator<DotTreeNode> iterator() {
		return children.iterator();
	}

	public void add(DotTreeNode child) {
		this.children.add(child);
	}

	public void visit(Consumer<DotTreeNode> visitor) {
		visitor.accept(this);
		forEach(c -> c.visit(visitor));
	}

	@Override
	public String toString() {
		return name;
	}

	public void print(StringBuilder sb) {
		print(sb, 0);
	}

	private void print(StringBuilder sb, int depth) {
		for (int i = 0; i < depth; i++)
			sb.append(" ");
		sb.append(name).append(System.lineSeparator());
		forEach(c -> c.print(sb, depth + 1));
	}

}