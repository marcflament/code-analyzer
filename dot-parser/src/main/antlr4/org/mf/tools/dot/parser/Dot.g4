grammar Dot;

graph
:
	DIGRAPH ID L_CURLY statement* R_CURLY
;

statement
:
	ID '->' ID STATEMENT_SEPARATOR
;

DIGRAPH
:
	'digraph'
;

L_CURLY
:
	'{'
;

STATEMENT_SEPARATOR
:
	';'
;

R_CURLY
:
	'}'
;

ID
:
	STRING
;

WS
:
	[ \t\r\n]+ -> skip
;

fragment
STRING
:
	'"'
	(
		~[\r\n"]
		| '\\"'
	)* '"'
;