package org.mf.tools.dot.parser;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.Test;
import org.mf.tools.ca.utils.io.SafeIOUtils;

public class DotTreeParserTest {
	
	@Test
	public void test() throws IOException, GraphParsingException {
		String string = loadResource();
		DotTreeNode root = DotTreeParser.parse(string);
		StringBuilder sb = new StringBuilder();
		root.print(sb);
		System.out.println(sb);
	}

	private String loadResource() throws IOException {
		try (InputStream is = DotTreeParserTest.class.getResourceAsStream("/tree.txt")) {
			return SafeIOUtils.toString(is, StandardCharsets.US_ASCII);
		}
	}

}
