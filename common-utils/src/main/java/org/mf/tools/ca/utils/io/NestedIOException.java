package org.mf.tools.ca.utils.io;

import java.io.IOException;

public class NestedIOException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NestedIOException(IOException cause) {
		super(cause);
	}

	public NestedIOException(String message, IOException cause) {
		super(message, cause);
	}

	public static <T> T safeCall(IOCallable<T> callable) {
		try {
			return callable.call();
		} catch (IOException e) {
			throw new NestedIOException(e);
		}
	}

	public static void safeRun(IORunnable runnable) {
		try {
			runnable.run();
		} catch (IOException e) {
			throw new NestedIOException(e);
		}
	}

}
