package org.mf.tools.ca.utils.io;

import java.io.IOException;

public interface IORunnable {
	void run() throws IOException;
}
