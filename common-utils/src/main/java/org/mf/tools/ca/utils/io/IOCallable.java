package org.mf.tools.ca.utils.io;

import java.io.IOException;

public interface IOCallable<T> {
	T call() throws IOException;
}
