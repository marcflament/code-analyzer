package org.mf.tools.ca.utils.io;

import java.io.File;
import java.io.IOException;

public class TempFileProvider {

	private TempFileProvider() {}

	public static File createTempFile() {
		return createTempFile(true);
	}

	public static File createTempFile(boolean deleteOnExit) {
		return createTempFile("tmp", deleteOnExit);
	}

	public static File createTempFile(String extension, boolean deleteOnExit) {
		return createTempFile("ca-", "." + extension, deleteOnExit);
	}

	public static File createTempFile(String prefix, String suffix, boolean deleteOnExit) {
		try {
			File res = File.createTempFile("ca-mvn", ".txt");
			if (deleteOnExit)
				res.deleteOnExit();
			return res;
		} catch (IOException e) {
			throw new NestedIOException(e);
		}
	}
}
