package org.mf.tools.ca.utils.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class creating temporary directory and deleting all its content on
 * exit.
 * 
 * @author Marc Flament
 * @created 2018-10-27
 */
public class TemDirectoryProvider {

	private static final List<File> tempDirectories = Collections.synchronizedList(new LinkedList<>());

	static {
		Runtime.getRuntime().addShutdownHook(new Thread(TemDirectoryProvider::deleteTempDirectories));
	}

	private TemDirectoryProvider() {}

	public static File createTempDirectory(String prefix) {
		File outputDiretory;
		try {
			outputDiretory = Files.createTempDirectory(prefix).toFile();
		} catch (IOException e) {
			throw new NestedIOException("Error creating temp directory", e);
		}
		tempDirectories.add(outputDiretory);
		return outputDiretory;
	}

	private static void deleteTempDirectories() {
		tempDirectories.forEach(d -> deleteDirectory(d));
	}

	private static void deleteDirectory(File f) {
		if (f.isDirectory()) {
			Arrays.asList(f.listFiles()).forEach(TemDirectoryProvider::deleteDirectory);
		} else {
			f.delete();
		}
		f.delete();
	}

}
