package org.mf.tools.ca.utils.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

public class SafeIOUtils {

	private SafeIOUtils() {}

	public static final String toString(File file) {
		return toString(file, Charset.defaultCharset());
	}

	public static final String toString(File file, Charset charset) {
		try (InputStream is = new FileInputStream(file)) {
			return IOUtils.toString(is, charset);
		} catch (IOException e) {
			throw new NestedIOException("Error reading string from " + file, e);
		}
	}

	public static final String toString(InputStream is, Charset charset) {
		try {
			return IOUtils.toString(is, charset);
		} catch (IOException e) {
			throw new NestedIOException(e);
		}
	}

}
